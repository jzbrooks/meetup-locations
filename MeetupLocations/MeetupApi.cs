﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MeetupLocations
{
	static class MeetupApi
	{
		public static string GetLocationData()
		{
			var client = new HttpClient();
			return Format(client.GetStringAsync(@"http://api.meetup.com/2/cities").Result);
		}


		private static string Format(string text)
		{
			var parsedJson = JsonConvert.DeserializeObject(text);
			return JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
		}
	}
}
