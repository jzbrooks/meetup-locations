﻿using System.Windows;

namespace MeetupLocations
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MeetupLocationsViewModel();
		}

		// Not MVVM!
		private void OnSelected(object sender, RoutedEventArgs e)
		{
			var location = LocationList.SelectedIndex;
			JsonText.ScrollToVerticalOffset((double)location * 191.5 + 30.0);
		}
	}
}
