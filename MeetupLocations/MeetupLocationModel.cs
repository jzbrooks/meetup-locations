﻿
namespace MeetupLocations
{
	class MeetupLocationModel
	{
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public string Country { get; set; }
		public double Distance { get; set; }
		public int MemberCount { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public int Id { get; set; }
		public int Ranking { get; set; }
	}
}
