﻿using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MeetupLocations
{
	class MeetupLocationsViewModel : INotifyPropertyChanged
	{
		#region properties
		private string locationsJsonText;
		public string LocationsJsonText
		{
			get { return locationsJsonText; }
			set 
			{
				if (value != locationsJsonText)
				{
					locationsJsonText = value;
					OnPropertyChanged();
				}
			}
		}

		private ObservableCollection<MeetupLocationModel> locationsList;
		public ObservableCollection<MeetupLocationModel> LocationsList
		{
			get { return locationsList; }
			set
			{
				if (value != locationsList)
				{
					locationsList = value;
					OnPropertyChanged();
				}
			}
		}
		#endregion

		public MeetupLocationsViewModel()
		{
			locationsList = new ObservableCollection<MeetupLocationModel>();

			DigestCloudData();
		}

		#region helper methods
		private void DigestCloudData()
		{
			LocationsJsonText = MeetupApi.GetLocationData();

			JObject locations = JObject.Parse(LocationsJsonText);
			JToken locationList = locations["results"];
			foreach (var token in locationList.ToList())
			{
				LocationsList.Add(new MeetupLocationModel { City = (string)token["city"],
															State = (string)token["state"],
															ZipCode = (string)token["zip"],
															Country = (string)token["country"],
															Distance = (int)token["distance"],
															MemberCount = (int)token["member_count"],
															Latitude = (double)token["lat"],
															Longitude = (double)token["lon"],
															Id = (int)token["id"],
															Ranking = (int)token["ranking"] });
			}
		}
		#endregion

		#region INotifyPropertyChanged
		public event PropertyChangedEventHandler PropertyChanged;
		private void OnPropertyChanged([CallerMemberName]string caller = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(caller));
			}
		}
		#endregion
	}
}
